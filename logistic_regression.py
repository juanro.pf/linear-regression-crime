import numpy as np
import matplotlib.pyplot as plt
import math
import random

# Modificar esta clase para seleccionar menos atributos (hacer más rápido el programa)
def get_points_without_nan_attributes(original_points):
  non_numeric_attributes= []
  for row in original_points:
    columna=-1
    for col in row:
      columna += 1
      if math.isnan(col) and not(columna in non_numeric_attributes):
        non_numeric_attributes.append(columna)
  non_numeric_attributes.sort()
  new_points= []
  for row in original_points:
    new_points.append([])
    fila= 0
  for row in original_points:
    columna= 0
    for col in row:
      if not(columna in non_numeric_attributes):
        new_points[fila].append(col)
      columna+= 1
    fila+= 1
  # En caso de que el label sea un porcentaje entre 0 y 1
  for row in new_points:
    for col in range(len(row)):
      if col == len(row) - 1:
        row[col]*= 100
  return new_points

def gen_test_list(points, training_vector):
  test_vector=[]
  for i in range(len(points)):
    if not(i in training_vector):
      test_vector.append(i)
  test_list= points[test_vector]
  return test_list

def init_tethas(points, tetha_initial_value= None):
  tethas=[]
  if tetha_initial_value == None:
    tethas_file= open('tethas_03_66.txt', 'r')
    for col in points[0]:
      tethas.append(float(tethas_file.readline()))
    tethas_file.close()
  elif tetha_initial_value == 0:
    for col in points[0]:
      if random.random() > .5:
        tethas.append(random.random())
      else:
        tethas.append(-1*random.random())
  else:
    for col in points[0]:
      if random.random() > .5:
        tethas.append(tetha_initial_value)
      else:
        tethas.append(-1*tetha_initial_value)
  return tethas

def gen_predictions (tethas, desired_list):
  predictions = []
  for row in desired_list:
    suma= 0
    cont= 0
    for col in row:
      suma += (tethas[cont] * col)
      cont+= 1
    predictions.append(suma)
  return predictions

def calculate_error(predictions, desired_list):
  error = 0
  length= len(desired_list)
  len_row= len(desired_list[0])
  row_cont= 0
  for row in desired_list:
    error += (row[len_row - 1] -     predictions [row_cont])**2
    row_cont+= 1
  MSE = error / length
  return MSE

def calc_accuracy(predictions, points):
  sums = 0
  cont= 0
  zeros= 0
  for row in points:
    if row[-1] < 1:
      zeros+= 1
    else:
      sums+= abs(((row[-1] - predictions[cont]) / row[-1]) * 100)
    # if row[-1] < predictions[cont]:
    #   if predictions[cont] < 1:
    #     # accuracy= row[-1] / 0.00001
    #     zeros+= 1
    #   else:
    #     accuracy= row[-1] / predictions[cont]
    #   sums+= abs(accuracy)
    # else:
    #   if row[-1] < 1:
    #     # accuracy= predictions[cont] / 0.00001
    #     zeros+= 1
    #   else:
    #     accuracy= predictions[cont] / row[-1]
    #   sums+= abs(accuracy)
    cont+= 1
  accuracy = sums / (len(points) - zeros)
  return accuracy

def show_error(errors_list):
  epochs= []
  for i in range(len(errors_list)):
    epochs.append(i)
  plt.plot(epochs, errors_list) 
  plt.xlabel('Epochs') 
  plt.ylabel('Error') 
  plt.title('Error chart') 
  plt.show()

def final_test(tethas, points):
  random_instance= round(random.random() * (len(points) - 1))
  print("Random instance to evaluate: " + str(random_instance))
  actual_value= points[random_instance][len(points[0]) - 1]
  print("Actual value: " + str(actual_value))
  predicted_value= 0
  for col in range(len(points[0])):
    predicted_value += (tethas[col] * points[random_instance][col])
  print("Predicted value: " + str(predicted_value))

# Generate points from file
original_points = np.genfromtxt("communities.data", dtype='float16', delimiter=",")
points= np.array(get_points_without_nan_attributes(original_points), dtype='float16')

# Define parameters
training_data_percentage= 0.8
# tetha_initial_value= 0.5
alpha = 0.0001

# Generate required 
# sums= np.zeros(len(points))
tethas= np.array(init_tethas(points, 0))
# tethas= np.array(init_tethas(points, tetha_initial_value))
training_vector= random.sample(range(0, len(points) - 1), round(len(points) * training_data_percentage))
training_list= points[training_vector]
test_list= gen_test_list(points, training_vector)
errors_list= []

# SUPER PROVISIONAL

# error= 10000
# omega= (alpha / (2 * len(training_list)))
# length_row= len(training_list[0])
# while error > 300:
#   sums= np.zeros(len(points))
#   tethas= np.array(init_tethas(points, 1))
#   for tetha in range(len(tethas)):
#     sums= 0
#     for row in training_list:
#       suma= np.sum(tethas * row)
#       suma-= (tethas[-1] * row[-1])
#       suma+= tethas[-1] - row[-1]
#       suma*= row[tetha]
#       sums+= suma
#     tethas[tetha]-= omega * sums
    
#   predictions= gen_predictions(tethas, training_list)
#   error = calculate_error(predictions, training_list)
#   if error < 300:
#     errors_list.append(error)
#   print(str(0) + ": " + str(error))

# SUPER PROVISIONAL

# Main program
# min_error= 1000000
omega= (alpha / (2 * len(training_list)))
length_row= len(training_list[0])
flag= True
for i in range(100):  #Cada epoch se generan nuevos tethas
  if flag:
    predictions= gen_predictions(tethas, training_list)
    min_error = calculate_error(predictions, training_list)
    flag= False
  for tetha in range(len(tethas)):
    sums= 0
    for row in training_list:
      suma= np.sum(tethas * row)
      suma-= (tethas[-1] * row[-1])
      suma+= tethas[-1] - row[-1]
      suma*= row[tetha]
      sums+= suma
    tethas[tetha]-= omega * sums
    # print("Tetha: " + str(tetha))
  
  predictions= gen_predictions(tethas, training_list)
  error = calculate_error(predictions, training_list)
  errors_list.append(error)
  print(str(i+1) + ": " + str(error))
  if(error <= min_error):
    min_error= error
    tethas_file= open('tethas.txt', 'w')
    for col in range(len(tethas)):
      if col == 0:
        tethas_file.write(str(tethas[col]))
      else:
        tethas_file.write("\n" + str(tethas[col]))
    tethas_file.close()
  else:
    break

# tethas= init_tethas(points)
final_training_predictions= gen_predictions(tethas, training_list)
final_training_error= calculate_error(final_training_predictions, training_list)
# final_training_accuracy= calc_accuracy(final_training_predictions, training_list)
print("Final training error:")
print(final_training_error)
# print("Final training accuracy:")
# print(final_training_accuracy)
final_test_predictions= gen_predictions(tethas, test_list)
final_test_error= calculate_error(final_test_predictions, test_list)
# final_test_accuracy= calc_accuracy(final_test_predictions, test_list)
print("Final tests error:")
print(final_test_error)
# print("Final test accuracy:")
# print(final_test_accuracy)
final_predictions= gen_predictions(tethas, points)
final_error= calculate_error(final_predictions, points)
# final_accuracy= calc_accuracy(final_predictions, points)
print("Final error:")
print(final_error)
# print("Final accuracy:")
# print(final_accuracy)
final_test(tethas, points)
final_test(tethas, points)
final_test(tethas, points)
final_test(tethas, points)
final_test(tethas, points)
show_error(errors_list)